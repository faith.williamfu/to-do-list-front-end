let recyclebin = [];
let filter = 'All';
function generateLiCode(item) {
  return '<li id="' + item.id + '"><input type="checkbox" onclick="checkStatus(this,' + item.id +')"' + (item.status === 'COMPLETED' ? 'checked' : '') + '/>' + (item.status === 'COMPLETED' ? '<s' : '<span') + ' ondblclick = "editable(this)" onkeydown = "uneditable(event, this)">' + item.text + (item.status === 'COMPLETED' ? '</s>' : '</span>') +'<button class="delete" onclick="deleteItem(' + item.id + ')"></button></li>';
}

function addItem() {
  let input = document.getElementById('inputbox').value;
  let newItem = {
    text: input,
    status: 'ACTIVE'
  }
  document.getElementById('inputbox').value = '';
  addNewItem(newItem, filter);
}

function enterKey(event) {
  if (event.keyCode === 13) {
    addItem();
  }
}

function allItemsButton() {
  filter = 'All';
  getAllItems(filter);
}

function activeItemsButton() {
  filter = 'Active';
  getAllItems(filter);
}

function completedItemsButton() {
  filter = 'Completed';
  getAllItems(filter);
}

function allItems(items) {
  let itemsElements = items.map((item) => generateLiCode(item)).join('');
  document.getElementById('list').innerHTML = itemsElements;
  selectedButton('buttonAll');
  unSelectedButton('buttonCompleted');
  unSelectedButton('buttonActive');
}

function activeItems(items) {
  let itemsElements = items.filter((items) => items.status === 'ACTIVE').map((item) => generateLiCode(item)).join('');
  document.getElementById('list').innerHTML = itemsElements;
  unSelectedButton('buttonCompleted');
  unSelectedButton('buttonAll');
  selectedButton('buttonActive');
}

function completedItems(items) {
  let itemsElements = items.filter((items) => items.status === 'COMPLETED').map((item) => generateLiCode(item)).join('');
  document.getElementById('list').innerHTML = itemsElements;
  selectedButton('buttonCompleted');
  unSelectedButton('buttonAll');
  unSelectedButton('buttonActive');
}

function checkStatus(checkbox, id) {
    let newItem = {
      id: id,
      text: checkbox.nextElementSibling.innerText,
      status: (checkbox.checked? 'COMPLETED' : 'ACTIVE')
    };
  updateItem(newItem, filter);
  }

function selectedButton(id) {
  document.getElementById(id).style.border = '1px solid #89c0f3';
  document.getElementById(id).style.backgroundColor = '#b7d9f8';
  document.getElementById(id).style.color = '#ffffff';
}

function unSelectedButton(id) {
  document.getElementById(id).style.border = 'none';
  document.getElementById(id).style.backgroundColor = 'buttonface';
  document.getElementById(id).style.color = '#89c0f3';
}

function editable(span) {
  span.setAttribute("contenteditable", true);
}

function uneditable(event, span) {
  if (event.keyCode === 13) {
    span.setAttribute("contenteditable", false);
    let newItem = {
      id: span.parentElement.attributes.id.value,
      text: span.innerText,
      status: (span.parentElement.firstChild.checked? 'COMPLETED' : 'ACTIVE')
    };
    updateItem(newItem, filter);
  }
}

function deleteItem(id) {
  let deletedItem = {
    id: id,
    text: document.getElementById(id).firstChild.nextSibling.innerText,
    status: (document.getElementById(id).firstChild.checked? 'COMPLETED' : 'ACTIVE')
  };
  recyclebin.push(deletedItem);
  deleteAItem(id, filter);
}

function undo() {
  if(recyclebin.length !== 0) {
    addNewItem(recyclebin[0], filter);
    recyclebin.shift();
  }
}

function filterStatus(filter, items) {
  if (filter == 'All') {
    allItems(items);
  }else if(filter == 'Completed') {
    completedItems(items);
  }else if (filter == 'Active') {
    activeItems(items);
  }
}