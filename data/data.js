function getAllItems(status) {
  fetch('http://localhost:8080/items')
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log(data);
      filterStatus(status, data);
    });
}

function addNewItem(item, status) {
  fetch('http://localhost:8080/items', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(item)
    })
    .then((response) => {
      return response.json();
    })
    .then(() => getAllItems(status));
}

function deleteAItem(id, status) {
  fetch('http://localhost:8080/items/' + id, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      },
    })
    .then(() => getAllItems(status));
}

function updateItem(item, stauts) {
  fetch('http://localhost:8080/items', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(item)
    })
    .then((response) => {
      return response.json();
    })
    .then(() => getAllItems(stauts));
}
